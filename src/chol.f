      SUBROUTINE ichol(A,N)
      
      !  Subroutine overwrites the sparse SPD matrix A
      ! with L from its IC factorization LL^T
       
      Implicit None
      
      Integer(4) :: i,j,k,N
      Real(8), Dimension(N,N) :: A
      
      Do k=1,N 
       A(k,k)=sqrt(A(k,k))
       Do i=k+1,N
        If (A(i,k).ne.0) Then
         A(i,k)=A(i,k)/A(k,k)
        End If
       End Do
       Do j=k+1,N
         Do i=j,N
          If (A(i,j).ne.0) Then
      	  A(i,j)=A(i,j)-A(i,k)*A(j,k)
      	End If
         End Do
       End Do
      End Do
      
      END SUBROUTINE
      
